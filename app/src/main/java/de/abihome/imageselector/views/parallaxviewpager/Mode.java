package de.abihome.imageselector.views.parallaxviewpager;

/**
 * Created by scally on 12/1/17.
 */

public enum Mode {

    LEFT_OVERLAY(0), RIGHT_OVERLAY(1), NONE(2);
    int value;

    Mode(int value) {
        this.value = value;
    }
}
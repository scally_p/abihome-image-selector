package de.abihome.imageselector.utils

class Constants {
    companion object {

        const val URL_DATA = "http://weyveed.herokuapp.com/test/images"
        const val TAB_LAST_IMAGE = 1
        const val TAB_PREV_IMAGE = 2

        //Broadcasts
        const val LOAD_GALLERY = "de.abihome.imageselector.LOAD_GALLERY"
        const val LOAD_IMAGE = "de.abihome.imageselector.LOAD_IMAGE"
    }
}
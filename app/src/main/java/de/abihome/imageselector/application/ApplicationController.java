package de.abihome.imageselector.application;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;

import de.abihome.imageselector.receiver.ConnectivityReceiver;
import jp.wasabeef.glide.transformations.BlurTransformation;

public class ApplicationController extends Application {

    private static ApplicationController application;

    @Override
    public void onCreate() {
        super.onCreate();

        application = this;
    }

    public static synchronized ApplicationController getInstance() {
        return application;
    }

    public static void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    /**
     * Glide Image Loader Instance for loading images across entire application
     */
    public void setImageWithGlide(final Context context, final String url, final ImageView imageView) {

        Log.d("Image Url", url);

        Glide.with(this)
                .load(url)
                .asBitmap()
                .transform(new BlurTransformation(context, 20))
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    public void onResourceReady(Bitmap drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);

                        Picasso.with(context)
                                .load(url) // image url goes here
//                                .resize(imageViewWidth, imageViewHeight)
                                .placeholder(imageView.getDrawable())
                                .into(imageView);
                    }
                });
    }
}

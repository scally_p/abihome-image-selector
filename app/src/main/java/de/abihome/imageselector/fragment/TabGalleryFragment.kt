package de.abihome.imageselector.fragment

import android.content.*
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import de.abihome.imageselector.R
import de.abihome.imageselector.application.ApplicationController
import de.abihome.imageselector.dao.Images
import de.abihome.imageselector.utils.Constants
import java.util.ArrayList

class TabGalleryFragment : Fragment(), View.OnClickListener {

    private val TAG = TabGalleryFragment::class.java.simpleName

    private lateinit var containerLayout: LinearLayout
    private lateinit var imagePreview: ImageView

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor
    private lateinit var application: ApplicationController
    private lateinit var loadGalleryBroadcastReceiver: LoadGalleryBroadcastReceiver

    private var lastImageIndex: Int = -1
    private var prevImageIndex: Int = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_tab_gallery, container, false)

        prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())
        edit = prefs.edit()
        application = activity!!.application as ApplicationController
        loadGalleryBroadcastReceiver = LoadGalleryBroadcastReceiver()

        containerLayout = bind(rootView, R.id.container)
        imagePreview = bind(rootView, R.id.image_view)

        val imageResults: String = prefs.getString("image_results", "")
        setData(imageResults)

        return rootView
    }

    private fun <T : View> bind(rootView: View, @IdRes res: Int): T {
        @Suppress("UNCHECKED_CAST")
        return rootView.findViewById<T>(res)
    }

    override fun onPause() {
        activity!!.unregisterReceiver(loadGalleryBroadcastReceiver)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(loadGalleryBroadcastReceiver, IntentFilter(Constants.LOAD_GALLERY))
    }

    override fun onClick(v: View?) {
        when (v?.id) {

        }
    }

    private fun setData(imageResults: String) {

        var imageList: ArrayList<String> = Images().getImages(imageResults)
        Log.d(TAG, "Image List Size: ${imageList.size}")

        if (lastImageIndex >= 0)
            application.setImageWithGlide(activity, imageList[lastImageIndex], imagePreview)

        if (containerLayout.childCount > 0) {
            containerLayout.removeAllViews()
        }

        for (i in 0 until imageList.size - 1) {

            Log.d(TAG, "Position - $i")

            var view: View = activity!!.layoutInflater.inflate(R.layout.layout_image_item, null)
            val cardView = view.findViewById<CardView>(R.id.card_view)
            val imageView = view.findViewById<ImageView>(R.id.image_view)

            application.setImageWithGlide(activity, imageList[i], imageView)

            cardView.setOnClickListener {
                Log.d(TAG, "Selected Image: Position - $i")
                Log.d(TAG, "Selected Image: Url - ${imageList[i]}")

                prevImageIndex = lastImageIndex
                lastImageIndex = i

                edit.putInt("last_image_index", lastImageIndex)
                edit.putInt("prev_image_index", prevImageIndex)
                edit.commit()
                activity!!.sendBroadcast(Intent(Constants.LOAD_IMAGE))

                application.setImageWithGlide(activity, imageList[i], imagePreview)
            }

            containerLayout.addView(view, i)
        }
    }

    private inner class LoadGalleryBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {

            Log.d(TAG, "Load Gallery Broadcast Received")

            val imageResults: String = prefs.getString("image_results", "")
            setData(imageResults)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment OnBoardingFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = TabGalleryFragment()
    }
}

package de.abihome.imageselector.fragment

import android.content.*
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import de.abihome.imageselector.R
import de.abihome.imageselector.application.ApplicationController
import de.abihome.imageselector.dao.Images
import de.abihome.imageselector.utils.Constants
import java.util.ArrayList


private const val TYPE = "type"

class TabSelectedImageFragment : Fragment(), View.OnClickListener {

    private val TAG = TabGalleryFragment::class.java.simpleName

    private lateinit var imagePreview: ImageView

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor
    private lateinit var application: ApplicationController
    private lateinit var loadImageBroadcastReceiver: LoadImageBroadcastReceiver

    private var type: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            type = it.getInt(TYPE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_tab_selected_image, container, false)

        prefs = PreferenceManager.getDefaultSharedPreferences(requireContext())
        edit = prefs.edit()
        application = activity!!.application as ApplicationController
        loadImageBroadcastReceiver = LoadImageBroadcastReceiver()

        imagePreview = bind(rootView, R.id.image_view)

        return rootView
    }

    private fun <T : View> bind(rootView: View, @IdRes res: Int): T {
        @Suppress("UNCHECKED_CAST")
        return rootView.findViewById<T>(res)
    }

    override fun onPause() {
        activity!!.unregisterReceiver(loadImageBroadcastReceiver)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(loadImageBroadcastReceiver, IntentFilter(Constants.LOAD_IMAGE))
        setData()
    }

    override fun onClick(v: View?) {
        when (v?.id) {

        }
    }

    private fun setData(){
        val imageResults: String = prefs.getString("image_results", "")
        val lastImageIndex: Int = prefs.getInt("last_image_index", -1)
        val prevImageIndex: Int = prefs.getInt("prev_image_index", -1)

        var imageList: ArrayList<String> = Images().getImages(imageResults)
        Log.d(TAG, "Image List Size: ${imageList.size}")

        when(type){
            Constants.TAB_LAST_IMAGE -> {
                if (lastImageIndex >= 0)
                    application.setImageWithGlide(activity, imageList[lastImageIndex], imagePreview)
            }
            Constants.TAB_PREV_IMAGE -> {
                if (prevImageIndex >= 0)
                    application.setImageWithGlide(activity, imageList[prevImageIndex], imagePreview)
            }
        }
    }

    private inner class LoadImageBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {

            Log.d(TAG, "Load Image Broadcast Received")
            setData()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: Int) =
                TabSelectedImageFragment().apply {
                    arguments = Bundle().apply {
                        putInt(TYPE, param1)
                    }
                }
    }
}

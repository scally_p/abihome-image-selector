package de.abihome.imageselector.dao

import android.util.Log
import org.json.JSONArray
import java.util.ArrayList

class Images {

    private val TAG = Images::class.java.simpleName

    fun getImages(imageResults: String): ArrayList<String> {
        val images = ArrayList<String>()
        try {

            var jsonArray = JSONArray(imageResults)
            for (i in 0 until jsonArray.length()) {

                var image: String = jsonArray.get(i).toString()
                Log.d(TAG, image)
                images.add(image)
            }

        } catch (e: Exception) {
            // TODO: handle except
            e.printStackTrace()
        }

        return images
    }

}
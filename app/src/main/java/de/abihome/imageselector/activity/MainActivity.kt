package de.abihome.imageselector.activity

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.annotation.IdRes
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import de.abihome.imageselector.R
import de.abihome.imageselector.fragment.TabGalleryFragment
import de.abihome.imageselector.fragment.TabSelectedImageFragment
import de.abihome.imageselector.receiver.ConnectivityReceiver
import de.abihome.imageselector.utils.Constants
import de.abihome.imageselector.views.parallaxviewpager.ParallaxViewPager
import org.json.JSONArray
import java.io.IOException
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    /**
     * UI Views Instances
     */
    private lateinit var toolbar: Toolbar
    private lateinit var toolbarTxt: TextView
    private lateinit var viewPager: ParallaxViewPager
    private lateinit var tabLayout: TabLayout

    /**
     * Class Instances
     */
    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor
    private lateinit var fragmentPageAdapter: FragmentPageAdapter
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()
        progressDialog = ProgressDialog(this)

        toolbar = bind(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        toolbarTxt = bind(R.id.title)
        viewPager = bind(R.id.viewpager)
        tabLayout = bind(R.id.tab_layout)

        setUpViewPager()
        setUpTabLayout()
        loadData()
    }

    private fun <T : View> Activity.bind(@IdRes res: Int): T {
        @Suppress("UNCHECKED_CAST")
        return findViewById<T>(res)
    }

    /**
     * Viewpager set set with appropriate Tabs
     */
    private fun setUpViewPager() {

        fragmentPageAdapter = FragmentPageAdapter(supportFragmentManager)
        fragmentPageAdapter.addFragment(TabGalleryFragment.newInstance(), R.drawable.tab_gallery_selected, resources.getString(R.string.gallery))
        fragmentPageAdapter.addFragment(TabSelectedImageFragment.newInstance(Constants.TAB_LAST_IMAGE), R.drawable.tab_last_image_unselected, resources.getString(R.string.last_image))
        fragmentPageAdapter.addFragment(TabSelectedImageFragment.newInstance(Constants.TAB_PREV_IMAGE), R.drawable.tab_prev_image_unselected, resources.getString(R.string.prev_image))

        viewPager.adapter = fragmentPageAdapter
    }

    private fun setUpTabLayout() {
        tabLayout.tabMode = TabLayout.MODE_FIXED
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout.setupWithViewPager(viewPager)
        for (i in 0 until tabLayout.tabCount) {
            val tab = tabLayout.getTabAt(i)
            tab!!.customView = fragmentPageAdapter.getTabView(i)
        }
        tabLayout.requestFocus()

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                setSelectedTab(tab)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                setUnSelectedTab(tab)
            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    /**
     * Selected Tab States
     */
    fun setSelectedTab(tab: TabLayout.Tab) {

        val view = tab.customView
        if (view != null) {
            val tabImage = view.findViewById<ImageView>(R.id.tab_image)
            val tabTitle = view.findViewById<TextView>(R.id.tab_title)
            tabTitle.setTextColor(resources.getColor(R.color.colorPrimary))

            when (tab.position) {

                0 -> {
                    tabImage.setImageResource(R.drawable.tab_gallery_selected)
                    tabTitle.text = resources.getString(R.string.gallery)
                }
                1 -> {
                    tabImage.setImageResource(R.drawable.tab_last_image_selected)
                    tabTitle.text = resources.getString(R.string.last_image)
                }
                2 -> {
                    tabImage.setImageResource(R.drawable.tab_prev_image_selected)
                    tabTitle.text = resources.getString(R.string.prev_image)
                }
            }
        }
    }

    /**
     * Unselected Tab States
     */
    fun setUnSelectedTab(tab: TabLayout.Tab) {

        val view = tab.customView
        if (view != null) {
            val tabImage = view.findViewById<ImageView>(R.id.tab_image)
            val tabTitle = view.findViewById<TextView>(R.id.tab_title)
            tabTitle.setTextColor(resources.getColor(R.color.cccccc))

            when (tab.position) {

                0 -> {
                    tabImage.setImageResource(R.drawable.tab_gallery_unselected)
                    tabTitle.text = resources.getString(R.string.gallery)
                }
                1 -> {
                    tabImage.setImageResource(R.drawable.tab_last_image_unselected)
                    tabTitle.text = resources.getString(R.string.last_image)
                }
                2 -> {
                    tabImage.setImageResource(R.drawable.tab_prev_image_unselected)
                    tabTitle.text = resources.getString(R.string.prev_image)
                }
            }
        }
    }

    private fun loadData() {

        var imageResults: String = prefs.getString("image_results", "")

        if (ConnectivityReceiver.isConnected()) {
            if (imageResults.isEmpty()) {
                getData()
            } else {
                sendBroadcast(Intent(Constants.LOAD_GALLERY))
            }
        } else {
            Toast.makeText(this, resources.getString(R.string.network_unavailable), Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Images fetched from server
     */
    private fun getData() {

        progressDialog.setMessage(resources.getString(R.string.fetching_please_wait))
        progressDialog.isIndeterminate = false
        progressDialog.setCancelable(false)
        progressDialog.show()

        val request = StringRequest(Request.Method.GET, Constants.URL_DATA,
                Response.Listener<String> { response ->

                    progressDialog.dismiss()

                    try {
                        val jsonArray = JSONArray(response)

                        edit.putString("image_results", jsonArray.toString())
                        edit.commit()

                        sendBroadcast(Intent(Constants.LOAD_GALLERY))

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }, Response.ErrorListener { error ->
            progressDialog.dismiss()
            error.printStackTrace()
        })

        Volley.newRequestQueue(this).add(request)
    }

    inner class FragmentPageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        private val tagFragments = ArrayList<Fragment>()
        private val tabIcons = ArrayList<Int>()
        private val tabTitles = ArrayList<String>()

        fun addFragment(fragment: Fragment, drawable: Int, title: String) {
            tagFragments.add(fragment)
            tabIcons.add(drawable)
            tabTitles.add(title)
        }

        override fun getItem(position: Int): Fragment {
            return tagFragments[position]
        }

        override fun getCount(): Int {
            return tagFragments.size
        }

        fun getTabView(position: Int): View {
            val tab = layoutInflater.inflate(R.layout.layout_tab_item, null)
            val tabImage = tab.findViewById<ImageView>(R.id.tab_image)
            val tabTitle = tab.findViewById<TextView>(R.id.tab_title)
            tabImage.setImageResource(tabIcons[position])
            tabTitle.text = tabTitles[position]
            if (position == 0) {
                tabImage.setImageResource(R.drawable.tab_gallery_selected)
                tabTitle.setTextColor(resources.getColor(R.color.colorPrimary))
            }
            return tab
        }
    }
}